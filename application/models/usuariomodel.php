<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class UsuarioModel extends CI_Model {

    var $db;
    
    public function __construct(){
        parent::__construct();
         $this->db = $this->load->database('default', true);
    }
    
    
    public function getUsuario($arrFiltro = ""){
        $this->db = $this->load->database('default', true);
        
        if (is_array($arrFiltro) && count($arrFiltro) > 0){            
            foreach ($arrFiltro as $key => $row){
                $where .= " AND {$key} {$row}";
            }
        }

        $sql = "
            SELECT
                *
            FROM
                user u
            WHERE 1
                {$where}
            ";
                
        return $this->db->query($sql);
    }
    
    
    public function setUsuario($arrData){        
        $this->db = $this->load->database('default', true);
                
        return $this->db->insert('user', $arrData);        
        
    }
    
    
    public function atualizaUsuario($arrSet, $arrWhere){
        $this->db = $this->load->database('default', true);
        
        if (is_array($arrWhere) && count($arrWhere) > 0){
            
            foreach ($arrWhere as $key => $row){                
                $this->db->where($key, $row);
            }
            
            $this->db->update('user', $arrSet);                
        }
        
    }
}