<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class RegistroModel extends CI_Model {

    var $db;
    
    public function __construct(){
        parent::__construct();
        
        $this->db = $this->load->database('default', true);
    }
    
    public function getRegistro($arrFiltro){
        
        if (is_array($arrFiltro) && count($arrFiltro) > 0){            
            foreach ($arrFiltro as $key => $row){
                $where .= " AND {$key} {$row}";
            }
        }        
        
        $sql = "
            SELECT
                qn.id AS questionnaire_id,
                qn.description AS question_title,
                q.id AS question_id,
                q.description As question,
                q.`type`,
                q.page_number
            FROM
                questionnaire qn
            INNER JOIN 	
                question q ON q.questionnaire_id = qn.id            
            WHERE 1
            {$where}
            ";
            
        return $this->db->query($sql);
        
    }
    
    
    public function setRegistro($arrData){
        
        $this->db = $this->load->database('default', true);
        
        $this->db->set('date_created', 'NOW()', false);        
        $this->db->insert('answer', $arrData);        
        
    }
    
    
    public function getQuestionOption($questionId){
        
        if (!empty($questionId)){
            
            $sql = "
                SELECT
                    *
                FROM
                    question_option qp
                WHERE
                    qp.question_id = {$questionId};                
                ";
            
            return $this->db->query($sql);
            
        }
        
    }
}