<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Registro extends CI_Controller {

    var $data;
    var $base_url;
    
    public function __construct(){
        parent::__construct();

        $this->load->library('session');
        $this->base_url = $this->config->item('base_url');
        
        if (!empty($this->session->userdata('id'))){
            
            $this->load->helper('url');
            redirect($this->base_url . 'index.php/page/', 'refresh');
        }else{
            
            $this->data["base_url"] = $this->base_url;
            $this->data["usuarioNome"] = $this->session->userdata('nome');
            
        }        
    }
    
    
    public function setCookie(){
        
        $this->load->model("RegistroModel");
        $this->load->helper('url');    
        
        
        $arrRegistroData = $this->input->post();        
        $this->session->set_userdata($arrRegistroData);
        
        if($arrRegistroData["page"] == 2){
            $this->gravar();
        }else{
            redirect($this->base_url . 'index.php/dashboard/index/2', 'refresh');
        }
        
        
        
    }
    
    
    public function gravar(){
        
        
        $arrQuestions = $this->session->userdata;
        
        foreach ($arrQuestions as $key => $row){
            
            $arrData["user_id"] = $this->session->userdata('usuarioId');
            
            $arrQuestion = explode("_", $key);
            if ($arrQuestion[0] == "question" && !empty($row)){
                $arrData["question_id"] = $arrQuestion[1];
                $arrData["question_option"] = $row;
                
                print_r($arrData);
                echo "<hr />";
                
                $this->RegistroModel->setRegistro($arrData);    
            }else{
                unset($arrData);
                continue;
            }
        }
        
        
        
        
    }
    
}