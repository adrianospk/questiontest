<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class DashBoard extends CI_Controller {

    var $data;
    var $base_url;
    
    public function __construct(){      
        
        parent::__construct();
               
        $this->load->library('session');
        $this->base_url = $this->config->item('base_url');
        
        if (!empty($this->session->userdata('id'))){
            
            $this->load->helper('url');
            redirect($this->base_url . 'index.php/page/', 'refresh');
        }else{
            
            $this->data["base_url"] = $this->base_url;
            $this->data["usuarioNome"] = $this->session->userdata('nome');
            
        }
    }

    public function index($page = 1){        
        
        $this->load->model('RegistroModel');
        
        $this->data["webroot"] = $this->config->item('webroot');
               
        $arrFiltro['qn.id'] = " = 1";
        $arrFiltro['q.page_number'] = " = " . $page;
        
        $this->data["page"] = $page;
        
        $objRegistro = $this->RegistroModel->getRegistro($arrFiltro);
                
        foreach ($objRegistro->result() as $key=>$row){
            
            $this->data['question_title'] = $row->question_title;
            
            $this->data['questions'][$key + 1]['question'] = $row->question;
            
            if ($row->type == 'enum'){
                $objQuestionOptions = $this->RegistroModel->getQuestionOption($row->question_id);
                
                foreach ($objQuestionOptions->result() as $ikey => $irow){
                    
                    $this->data['questions'][$key + 1]['options'][$ikey]['text'] = $irow->description;
                    
                    $valueField = ($irow->field_type == "text") ? "" : $irow->id;
                    $htmlField = '<input type="' . $irow->field_type . '" id="question_' . $row->question_id . '" name="question_' . $row->question_id . '" value="' . $valueField . '" />';
                
                    $this->data['questions'][$key + 1]['options'][$ikey]['html'] = $htmlField;
                    
                }
            }
            
        }
            
        $this->load->view('dashboard', $this->data);
        
    }
    
    
    public function gravar(){
        
        
        
    }
    
}