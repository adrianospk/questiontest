<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuario extends CI_Controller {

    public function __construct(){
        parent::__construct();
        
    }
 
    
    public function cadastrar(){

        $this->load->model('UsuarioModel');
        
        $data["base_url"] = $this->config->item('base_url');        
               
        $dataForm["firstname"] = $this->input->post("firstname", true);
        $dataForm["lastname"] = $this->input->post("lastname", true);
        $dataForm["email"] = $this->input->post("email", true);
        $dataForm["password"] = $this->input->post("password", true); //criar metodo para melhorar a criptografia da senha
        
        // verifica se o e-mail ja esta cadastrado
        $objEmailUsuario = $this->UsuarioModel->getUsuario(array("u.email" => " = '{$dataForm["email"]}'"));
        
        if ($objEmailUsuario->num_rows() > 0){
            
            // E-Mail já existe, solicitar que recupere a senha;
            
        }else{
        
            if (false === array_search(false , $dataForm, false)){

                $dataForm["password"] = md5($dataForm["password"]);
                $usuarioId = $this->UsuarioModel->setUsuario($dataForm);
                
                //Enviaar e-mail com o link de ativação.
                
                echo '< href="' . $data["base_url"] . '"index.php/usuario/validacadastro/"' . $usuarioId . '" target="_blank">Click to activate your account</a>';
                
            }else{
                // Não preencheram os dados básico do form. Tiraram o validate.
            }
            
        }
        
    }
    
    
    
    public function validaCadastro($usuarioId){
        
        $this->load->model('UsuarioModel');
        
        $objUsuario = $this->UsuarioModel->getUsuario(array("u.id" => " = '{$usuarioId}'"));
        $objUsuario = $objUsuario->row();
        
        if ($objUsuario->is_validate == 0){            
            $this->UsuarioModel->atualizaUsuario(array("is_validate" => 1), array("id" => $usuarioId));
        }else{
            
            // Informar que o link já havia sido validado.
            
        }
        
    }
    
    
    
    public function logar(){
        
        $this->load->model('UsuarioModel');
        
        $data["base_url"] = $this->config->item('base_url');
        
        $email = $this->input->post("email", true);
        $password = $this->input->post("password", true);
        
        $arrFiltro["u.email"] = " = '{$email}'";
        $arrFiltro["u.password"] = " = '" . md5($password) . "'";
        
        $objUsuario = $this->UsuarioModel->getUsuario($arrFiltro);
        $objUsuario = $objUsuario->row();        
        
        if ($objUsuario->is_validate == 0){
            echo "Avisa que ele ainda não ativou o e-mail";
            
        }else{
            $this->load->library('session');
            $this->load->helper('url');
            
            $this->session->set_userdata('usuarioId', $objUsuario->id);
            $this->session->set_userdata('nome', $objUsuario->firstname);
            redirect($data["base_url"] . 'index.php/dashboard/', 'refresh');
        }
        
    }
    
    public function sair(){
        $this->load->helper('url');
        $this->load->library('session');
        $this->session->sess_destroy();
        redirect($data["base_url"], 'refresh');
    }
    
}